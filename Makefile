.PHONY: clean

CFLAGS  := -Wall -Wno-error -g
LD      := gcc
LDLIBS  := ${LDLIBS} -lrdmacm -libverbs -lpthread

APPS    := rdma-client rdma-server mm mr

all: ${APPS}

rdma-client: client.o
	${LD} -o $@ $^ ${LDLIBS}

rdma-server: server.o
	${LD} -o $@ $^ ${LDLIBS}

mm: mm2.o
	${LD} -o $@ $^ ${LDLIBS}

mr: mm_read.o
	${LD} -o $@ $^ ${LDLIBS}

clean:
	rm -f *.o ${APPS}