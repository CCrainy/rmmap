#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <infiniband/verbs.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <endian.h>
#include <byteswap.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h> /* mmap() is defined in this header */
#include <fcntl.h>

#define msg "hello,this is client!"
#define MSG_SIZE (strlen(msg) + 1)

#if __BYTE_ORDER == __LITTLE_ENDIAN
static inline uint64_t htonll(uint64_t x) { return bswap_64(x); }
static inline uint64_t ntohll(uint64_t x) { return bswap_64(x); }
#elif __BYTE_ORDER == __BIG_ENDIAN
static inline uint64_t htonll(uint64_t x) { return x; }
static inline uint64_t ntohll(uint64_t x) { return x; }
#else
#error __BYTE_ORDER is neither __LITTLE_ENDIAN nor __BIG_ENDIAN
#endif

struct resource
{
    char *dev;
    struct ibv_context *ctx;
    struct ibv_pd *pd;
    struct ibv_cq *cq;
    struct ibv_qp_init_attr *attr;
    struct ibv_qp *qp;
    struct ibv_mr *mr;
    struct ibv_port_attr port_attr;
    int sockfd;
    char *buf;
};

struct conn_data
{
    uint64_t addr;   /* Buffer address */
    uint32_t rkey;   /* Remote key */
    uint16_t lid;    /* LID of the IB port */
    uint32_t qp_num;
};

void sock_sync_data(int sockfd, int length, char *send_data, char *recv_data)
{
    write(sockfd, send_data, length);
    int total_read_byte = 0;
    while (total_read_byte < length)
    {
        int read_byte = read(sockfd, recv_data, length);
        if (read_byte > 0)
            total_read_byte += read_byte;
        else
            break;
    }
}

static int resources_destroy(struct resource *res)
{
    int rc = 0;
    if (res->qp)
        if (ibv_destroy_qp(res->qp))
        {
            fprintf(stderr, "failed to destroy QP\n");
            rc = 1;
        }
    if (res->mr)
        if (ibv_dereg_mr(res->mr))
        {
            fprintf(stderr, "failed to deregister MR\n");
            rc = 1;
        }
    if (res->buf)
        free(res->buf);
    if (res->cq)
        if (ibv_destroy_cq(res->cq))
        {
            fprintf(stderr, "failed to destroy CQ\n");
            rc = 1;
        }
    if (res->pd)
        if (ibv_dealloc_pd(res->pd))
        {
            fprintf(stderr, "failed to deallocate PD\n");
            rc = 1;
        }
    if (res->ctx)
        if (ibv_close_device(res->ctx))
        {
            fprintf(stderr, "failed to close device context\n");
            rc = 1;
        }
    if (res->sockfd >= 0)
        if (close(res->sockfd))
        {
            fprintf(stderr, "failed to close socket\n");
            rc = 1;
        }
    return rc;
}


int post_send(struct resource *res, int opcode, struct conn_data *remote)
{
	struct ibv_send_wr sr;
	struct ibv_sge sge;
	struct ibv_send_wr *bad_wr = NULL;
	int rc;
	/* prepare the scatter/gather entry */
	memset(&sge, 0, sizeof(sge));
	sge.addr = (uintptr_t)res->buf;
	sge.length = MSG_SIZE;
	sge.lkey = res->mr->lkey;
	/* prepare the send work request */
	memset(&sr, 0, sizeof(sr));
	sr.next = NULL;
	sr.wr_id = 0;
	sr.sg_list = &sge;
	sr.num_sge = 1;
	sr.opcode = opcode;
	sr.send_flags = IBV_SEND_SIGNALED;
	if (opcode != IBV_WR_SEND)
	{
		sr.wr.rdma.remote_addr = remote->addr;
		sr.wr.rdma.rkey = remote->rkey;
	}
	/* there is a Receive Request in the responder side, so we won't get any into RNR flow */
	rc = ibv_post_send(res->qp, &sr, &bad_wr);
	if (rc)
		fprintf(stderr, "failed to post SR\n");
	else
	{
		switch (opcode)
		{
		case IBV_WR_SEND:
			fprintf(stdout, "Send Request was posted\n");
			break;
		case IBV_WR_RDMA_READ:
			fprintf(stdout, "RDMA Read Request was posted\n");
			break;
		case IBV_WR_RDMA_WRITE:
			fprintf(stdout, "RDMA Write Request was posted\n");
			break;
		default:
			fprintf(stdout, "Unknown Request was posted\n");
			break;
		}
	}
	return rc;
}


int poll_completion(struct resource *res){
    struct ibv_wc wc;
    unsigned long start_time_msec;
	unsigned long cur_time_msec;
	struct timeval cur_time;
	int poll_result;
	int rc = 0;
    gettimeofday(&cur_time, NULL);
	start_time_msec = (cur_time.tv_sec * 1000) + (cur_time.tv_usec / 1000);

    do
    {
        poll_result =ibv_poll_cq(res->cq,1,&wc);
        gettimeofday(&cur_time, NULL);
		cur_time_msec = (cur_time.tv_sec * 1000) + (cur_time.tv_usec / 1000);

    } while ((poll_result == 0) && ((cur_time_msec - start_time_msec) < 2000));
    	if (poll_result < 0)
	{
		/* poll CQ failed */
		fprintf(stderr, "poll CQ failed\n");
		rc = 1;
	}
	else if (poll_result == 0)
	{ /* the CQ is empty */
		fprintf(stderr, "completion wasn't found in the CQ after timeout\n");
		rc = 1;
	}
	else
	{
		/* CQE found */
		fprintf(stdout, "completion was found in CQ with status 0x%x\n", wc.status);
		/* check the completion status (here we don't care about the completion opcode */
		if (wc.status != IBV_WC_SUCCESS)
		{
			fprintf(stderr, "got bad completion with status: 0x%x, vendor syndrome: 0x%x\n", wc.status,
					wc.vendor_err);
			rc = 1;
		}
	}
	return rc;

}

int main(int argc, char *argv[])
{
    int num_dev;
    struct ibv_device **dev_list;
    struct resource *res = malloc(sizeof(struct resource));
    dev_list = ibv_get_device_list(&num_dev);
    res->dev = strdup(ibv_get_device_name(dev_list[0]));
    res->ctx = ibv_open_device(dev_list[0]);
    printf("No.0 device's name is %s\n", res->dev);
    ibv_query_port(res->ctx, 1, &res->port_attr);
    res->pd = ibv_alloc_pd(res->ctx);
    res->cq = ibv_create_cq(res->ctx, 10, NULL, NULL, 0);

    res->attr = (struct ibv_qp_init_attr *)malloc(sizeof(struct ibv_qp_init_attr));
    memset(res->attr,0,sizeof(*res->attr));
    res->attr->recv_cq = res->attr->send_cq = res->cq;
    res->attr->qp_type = IBV_QPT_RC;
    res->attr->sq_sig_all = 1;
    res->attr->cap.max_recv_sge = 4;
    res->attr->cap.max_recv_wr = 4;
    res->attr->cap.max_send_sge = 4;
    res->attr->cap.max_send_wr = 4;

    res->qp = ibv_create_qp(res->pd, res->attr);

    res->buf = (char *)malloc(MSG_SIZE);
    memset(res->buf, 0, MSG_SIZE);

    strcpy(res->buf, msg);

    res->mr = ibv_reg_mr(res->pd, res->buf, MSG_SIZE, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE);

    struct addrinfo *resolved_addr = NULL;
    struct addrinfo *iterator;
    struct addrinfo hints = {
        .ai_flags = AI_PASSIVE,
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM};

    int t = 0;
    int sockfd = 0;
    char *sername = strdup(argv[1]);
    char *sport = "2000";
    t = getaddrinfo(sername, sport, &hints, &resolved_addr);
    for (iterator = resolved_addr; iterator; iterator = iterator->ai_next)
    {
        sockfd = socket(iterator->ai_family, iterator->ai_socktype, iterator->ai_protocol);
        if (sockfd >= 0)
        {
            if ((t = connect(sockfd, iterator->ai_addr, iterator->ai_addrlen)))
            {
                fprintf(stdout, "failed connect \n");
                close(sockfd);
                sockfd = -1;
            }
        }
    }



    res->sockfd = sockfd;
    char temp_char;
    sock_sync_data(res->sockfd, sizeof(char), "w", &temp_char);    
    printf("test char is %c\n:",temp_char);
    
    struct conn_data local_data, remote_data, temp_data;
    local_data.addr = htonll((uintptr_t)res->buf);
    local_data.rkey = htonl(res->mr->rkey);
    local_data.lid = htons(res->port_attr.lid);
    local_data.qp_num = htonl(res->qp->qp_num);
    fprintf(stdout, "local address = 0x%" PRIx64 "\n", (uintptr_t)res->buf);
    fprintf(stdout, "local rkey = 0x%x\n", res->mr->rkey);
    fprintf(stdout, "local LID = 0x%x\n", res->port_attr.lid);
    fprintf(stdout, "local QP number = 0x%x\n", res->qp->qp_num);

    //sock_sync_data(res->sockfd, sizeof(struct conn_data), (char *)&local_data, (char *)&temp_data);
    sock_sync_data(res->sockfd, sizeof(local_data.addr), (char *)&local_data.addr, (char *)&temp_data.addr);
    sock_sync_data(res->sockfd, sizeof(local_data.rkey), (char *)&local_data.rkey, (char *)&temp_data.rkey);
    sock_sync_data(res->sockfd, sizeof(local_data.lid), (char *)&local_data.lid, (char *)&temp_data.lid);
    sock_sync_data(res->sockfd, sizeof(local_data.qp_num), (char *)&local_data.qp_num, (char *)&temp_data.qp_num);
    remote_data.addr = ntohll(temp_data.addr);
    remote_data.rkey = ntohl(temp_data.rkey);
    remote_data.lid = ntohs(temp_data.lid);
    remote_data.qp_num = ntohl(temp_data.qp_num);

    fprintf(stdout, "Remote address = 0x%" PRIx64 "\n", remote_data.addr);
    fprintf(stdout, "Remote rkey = 0x%x\n", remote_data.rkey);
    fprintf(stdout, "Remote LID = 0x%x\n", remote_data.lid);
    fprintf(stdout, "Remote QP number = 0x%x\n", remote_data.qp_num);


    struct ibv_qp_attr qattr;
    
    int flags=0;
    memset(&qattr, 0, sizeof(qattr));
 
    qattr.qp_state        = IBV_QPS_INIT;
    qattr.pkey_index      = 0;
    qattr.port_num        = 1;
    qattr.qp_access_flags = IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE;

    flags = IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS;
    if(ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to INIT\n");
	    return -1;
    }

    flags=0;
    memset(&qattr, 0, sizeof(qattr));

    qattr.qp_state = IBV_QPS_RTR;
    qattr.path_mtu = IBV_MTU_512;
    qattr.dest_qp_num = remote_data.qp_num;
	qattr.rq_psn = 0;
	qattr.max_dest_rd_atomic = 1;
	qattr.min_rnr_timer = 0x12;
	qattr.ah_attr.is_global = 0;
	qattr.ah_attr.dlid = remote_data.lid;
	qattr.ah_attr.sl = 0;
	qattr.ah_attr.src_path_bits = 0;
	qattr.ah_attr.port_num = 1;

    flags= IBV_QP_STATE | IBV_QP_AV |IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER;
    if(ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to RTR\n");
	    return -1;
    }

    memset(&qattr,0,sizeof(qattr));
    flags=0;

    qattr.qp_state = IBV_QPS_RTS;
	qattr.timeout = 0x12;
	qattr.retry_cnt = 6;
	qattr.rnr_retry = 6;
	qattr.sq_psn = 0;
	qattr.max_rd_atomic = 1;
	flags = IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT |
			IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC;

    if(ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to RTS\n");
	    return -1;
    }

    post_send(res, IBV_WR_RDMA_READ, &remote_data);
    if (poll_completion(res))
	{
		fprintf(stderr, "poll completion failed 2\n");
	}
	fprintf(stdout, "Contents of server's buffer: '%s'\n", res->buf);

    strcpy(res->buf,"cell2,this is client!");
    post_send(res, IBV_WR_RDMA_WRITE, &remote_data);
    if (poll_completion(res))
	{
		fprintf(stderr, "poll completion failed 3\n");
	}
    resources_destroy(res);
    return 0;

}
