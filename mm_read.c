#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h> /* mmap() is defined in this header */
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
void err_quit(char *msg)
{
    printf(msg);
    return ;
}


int main (int argc, char *argv[])
{
 int fdin;
 char *src;
 struct stat statbuf;

 /* open the input file */
 if ((fdin = open ("test_file", O_RDONLY)) < 0)
   {printf("can't open %s for reading", "test_file");
    return 0;
   }


 /* find size of input file */
 if (fstat (fdin,&statbuf) < 0)
   {printf ("fstat error");
    return 0;
   }


 /* mmap the input file */
 if ((src = mmap (0, statbuf.st_size, PROT_READ, MAP_SHARED, fdin, 0))
   == (caddr_t) -1)
   {printf ("mmap error for input");
    return 0;
   }

 printf("%s\n", src);
 return 0;

}