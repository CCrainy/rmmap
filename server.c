#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <infiniband/verbs.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <endian.h>
#include <byteswap.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h> /* mmap() is defined in this header */
#include <fcntl.h>

#define msg "hello,this is server!"
#define MSG_SIZE (strlen(msg) + 1)

#if __BYTE_ORDER == __LITTLE_ENDIAN
static inline uint64_t htonll(uint64_t x) { return bswap_64(x); }
static inline uint64_t ntohll(uint64_t x) { return bswap_64(x); }
#elif __BYTE_ORDER == __BIG_ENDIAN
static inline uint64_t htonll(uint64_t x) { return x; }
static inline uint64_t ntohll(uint64_t x) { return x; }
#else
#error __BYTE_ORDER is neither __LITTLE_ENDIAN nor __BIG_ENDIAN
#endif

struct resource
{
    char *dev;
    struct ibv_context *ctx;
    struct ibv_pd *pd;
    struct ibv_cq *cq;
    struct ibv_qp_init_attr *attr;
    struct ibv_qp *qp;
    struct ibv_mr *mr;
    struct ibv_port_attr port_attr;
    int sockfd;
    char *buf;
};

struct conn_data
{
    uint64_t addr; /* Buffer address */
    uint32_t rkey; /* Remote key */
    uint32_t qp_num;
    uint16_t lid; /* LID of the IB port */
};

void sock_sync_data(int sockfd, int length, char *send_data, char *recv_data)
{
    write(sockfd, send_data, length);
    int total_read_byte = 0;
    while (total_read_byte < length)
    {
        int read_byte = read(sockfd, recv_data, length);
        if (read_byte > 0)
            total_read_byte += read_byte;
        else
            break;
    }
}

static int resources_destroy(struct resource *res)
{
    int rc = 0;
    if (res->qp)
        if (ibv_destroy_qp(res->qp))
        {
            fprintf(stderr, "failed to destroy QP\n");
            rc = 1;
        }
    if (res->mr)
        if (ibv_dereg_mr(res->mr))
        {
            fprintf(stderr, "failed to deregister MR\n");
            rc = 1;
        }
    if (res->buf){
        msync(res->buf, MSG_SIZE, MS_SYNC);
        munmap(res->buf, MSG_SIZE);
        free(res->buf);
    }
    if (res->cq)
        if (ibv_destroy_cq(res->cq))
        {
            fprintf(stderr, "failed to destroy CQ\n");
            rc = 1;
        }
    if (res->pd)
        if (ibv_dealloc_pd(res->pd))
        {
            fprintf(stderr, "failed to deallocate PD\n");
            rc = 1;
        }
    if (res->ctx)
        if (ibv_close_device(res->ctx))
        {
            fprintf(stderr, "failed to close device context\n");
            rc = 1;
        }
    if (res->sockfd >= 0)
        if (close(res->sockfd))
        {
            fprintf(stderr, "failed to close socket\n");
            rc = 1;
        }
    return rc;
}

int main(int argc, char *argv[])
{

    int num_dev;
    struct ibv_device **dev_list = NULL;
    struct resource *res = malloc(sizeof(struct resource));
    dev_list = ibv_get_device_list(&num_dev);
    res->dev = strdup(ibv_get_device_name(dev_list[0]));
    printf("No.0 device's name is %s\n", res->dev);
    res->ctx = ibv_open_device(dev_list[0]);
    ibv_query_port(res->ctx, 1, &res->port_attr);
    res->pd = ibv_alloc_pd(res->ctx);
    res->cq = ibv_create_cq(res->ctx, 10, NULL, NULL, 0);

    int fd = open("test_file", O_RDWR | O_CREAT, (mode_t)0664);
    if(fd < 0)
    {
     printf ("can't create %s for writing", argv[2]);
     return 0;
    }

    res->buf = mmap ((void *)NULL, MSG_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    //lseek(fd, MSG_SIZE, SEEK_SET);
    //write(fd, "", 1);
    posix_fallocate(fd, 0, (off_t)MSG_SIZE);
    memset(res->buf, 0, MSG_SIZE);
    strcpy(res->buf, msg);
    res->mr = ibv_reg_mr(res->pd, res->buf, MSG_SIZE, IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE);

    res->attr = (struct ibv_qp_init_attr *)malloc(sizeof(struct ibv_qp_init_attr));
    memset(res->attr, 0, sizeof(*res->attr));
    res->attr->recv_cq = res->attr->send_cq = res->cq;

    res->attr->qp_type = IBV_QPT_RC;

    res->attr->sq_sig_all = 1;

    res->attr->cap.max_recv_sge = 4;

    res->attr->cap.max_recv_wr = 4;

    res->attr->cap.max_send_sge = 4;

    res->attr->cap.max_send_wr = 4;

    res->qp = ibv_create_qp(res->pd, res->attr);

    struct addrinfo *resolved_addr = NULL;
    struct addrinfo *iterator;
    struct addrinfo hints = {
        .ai_flags = AI_PASSIVE,
        .ai_family = AF_INET,
        .ai_socktype = SOCK_STREAM};

    int t = 0;
    int sockfd = 0;
    int listenfd = 0;
    char *sport = "2000";
    t = getaddrinfo(NULL, sport, &hints, &resolved_addr);

    for (iterator = resolved_addr; iterator; iterator = iterator->ai_next)
    {
        listenfd = socket(iterator->ai_family, iterator->ai_socktype, iterator->ai_protocol);
        if (sockfd >= 0)
        {

            if ((t = bind(listenfd, iterator->ai_addr, iterator->ai_addrlen)))
            {
                fprintf(stdout, "failed bind \n");
                close(sockfd);
                sockfd = -1;
            }
            listen(listenfd, 4);
            sockfd = accept(listenfd, NULL, 0);
        }
    }
    res->sockfd = sockfd;
    char temp_char;
    sock_sync_data(res->sockfd, sizeof(char), "w", &temp_char);    
    printf("test char is %c\n:",temp_char);

    struct conn_data local_data, remote_data, temp_data;
    local_data.addr = htonll((uintptr_t)res->buf);
    local_data.rkey = htonl(res->mr->rkey);
    local_data.lid = htons(res->port_attr.lid);
    local_data.qp_num = htonl(res->qp->qp_num);
    
    fprintf(stdout, "local address = 0x%" PRIx64 "\n", (uintptr_t)res->buf);
    fprintf(stdout, "local rkey = 0x%x\n", res->mr->rkey);
    fprintf(stdout, "local LID = 0x%x\n", res->port_attr.lid);
    fprintf(stdout, "local QP number = 0x%x\n", res->qp->qp_num);

    //sock_sync_data(res->sockfd, sizeof(struct conn_data), (char *)&local_data, (char *)&temp_data);
    sock_sync_data(res->sockfd, sizeof(local_data.addr), (char *)&local_data.addr, (char *)&temp_data.addr);
    sock_sync_data(res->sockfd, sizeof(local_data.rkey), (char *)&local_data.rkey, (char *)&temp_data.rkey);
    sock_sync_data(res->sockfd, sizeof(local_data.lid), (char *)&local_data.lid, (char *)&temp_data.lid);
    sock_sync_data(res->sockfd, sizeof(local_data.qp_num), (char *)&local_data.qp_num, (char *)&temp_data.qp_num);
    remote_data.addr = ntohll(temp_data.addr);
    remote_data.rkey = ntohl(temp_data.rkey);
    remote_data.lid = ntohs(temp_data.lid);
    remote_data.qp_num = ntohl(temp_data.qp_num);

    fprintf(stdout, "Remote address = 0x%" PRIx64 "\n", remote_data.addr);
    fprintf(stdout, "Remote rkey = 0x%x\n", remote_data.rkey);
    fprintf(stdout, "Remote LID = 0x%x\n", remote_data.lid);
    fprintf(stdout, "Remote QP number = 0x%x\n", remote_data.qp_num);

    struct ibv_qp_attr qattr;

    int flags = 0;
    memset(&qattr, 0, sizeof(qattr));

    qattr.qp_state = IBV_QPS_INIT;
    qattr.pkey_index = 0;
    qattr.port_num = 1;
    qattr.qp_access_flags = IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE;

    flags = IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS;
    if (ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to INIT\n");
        return -1;
    }

    flags = 0;
    memset(&qattr, 0, sizeof(qattr));

    qattr.qp_state = IBV_QPS_RTR;
    qattr.path_mtu = IBV_MTU_512;
    qattr.dest_qp_num = remote_data.qp_num;
    qattr.rq_psn = 0;
    qattr.max_dest_rd_atomic = 1;
    qattr.min_rnr_timer = 0x12;
    qattr.ah_attr.is_global = 0;
    qattr.ah_attr.dlid = remote_data.lid;
    qattr.ah_attr.sl = 0;
    qattr.ah_attr.src_path_bits = 0;
    qattr.ah_attr.port_num = 1;

    flags = IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER;
    if (ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to RTR\n");
        return -1;
    }

    memset(&qattr, 0, sizeof(qattr));
    flags = 0;

    qattr.qp_state = IBV_QPS_RTS;
    qattr.timeout = 0x12;
    qattr.retry_cnt = 6;
    qattr.rnr_retry = 6;
    qattr.sq_psn = 0;
    qattr.max_rd_atomic = 1;
    flags = IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT |
            IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC;

    if (ibv_modify_qp(res->qp, &qattr, flags))
    {
        fprintf(stderr, "Failed to modify QP to RTS\n");
        return -1;
    }

    while (1)
    {
        if (res->buf[0] == 'c')
        {
            fprintf(stdout, "Contents of client's buffer: '%s'\n", res->buf);
            break;
        }
    }

    resources_destroy(res);
    return 0;
}
